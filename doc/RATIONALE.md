# Rationale for an *Spatialization Engine Creator* library

## Introduction

Spatialization engines usually involve a huge number of variables and
repetitive code operations. Some older music languages like `csound` or even
more modern ones like `faust` do not have the necessary tooling to represent
complex sound space configurations (i.e. multiple speakers, multiple sources,
multiple movements, etc.) properly. Thus, a properly instrumented OOP library
may fulfill two different tasks:

1. represent a complex configuration in a simple way
1. being language agnostic and thus language-independent, with different
   backend providing for representation

## Object tree

Here's a tentative description of the objects involved in the library:

* `Coord`
* `Room`
* `Speaker`
* `Source`
* `Movement`
* `Driver`
  * `csound`
  * `sc`
  * `pd`
  * `faust`

GUI objects will be developed at a later stage.
